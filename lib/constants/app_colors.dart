import 'dart:ui';

import 'package:hexcolor/hexcolor.dart';

class AppColors {
  static Color appPrimaryColor = HexColor("#FF4157FF");
  static const primaryColorold = Color(0xFF4157FF);
  static const secondaryColor1 = Color(0xFF50D1F6);
  static const secondaryColor2 = Color(0xFFFFCA22);
  static const secondaryColor3 = Color(0xFF3DD902);
  static const secondaryColor4 = Color(0xFFFA1225);
  static const secondaryColor5 = Color(0xFF49493A);

  static const primaryGradientColors = [secondaryColor1, primaryColorold];

  static const mapPrimaryMarkerColor = Color(0xFF4E94BF);
}
const primaryColorNew = Color(0xFF4157FF);
const textColor = Color(0x99090F47);
const backgroundIcon = Color(0xFFA0ABFF);
const backgroundIcon2 = Color(0xFFDFE3FF);
const colorCard = Color(0xFFF5F7FA);
const textColorblack = Color(0xFF000000);
const textColorWiht = Color(0xFFFFFFFF);