import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomText extends StatelessWidget {
  String text;
  double height;
  int? maxLines;
  TextOverflow? textOverflow;
  TextDecoration? decoration;
  Color? colorDecoration;


  CustomText(
      {Key? key,
      required this.text,
      this.decoration,
        this.colorDecoration,
      this.textAlign = TextAlign.justify,
      this.color = Colors.white,
      required this.fontSize,
      this.fontWeight = FontWeight.normal,
      this.height = 1,
      this.maxLines = 2,
      this.textOverflow})
      : super(key: key);

  Color color;
  double fontSize;
  FontWeight fontWeight;
  TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: fontSize,
        fontWeight: fontWeight,
        decoration: decoration,
        decorationColor: colorDecoration,
        decorationThickness: 8,
        decorationStyle:
        TextDecorationStyle.solid,
        height: height,
      ),
      textAlign: textAlign,
      maxLines: maxLines,
      overflow: textOverflow,
      softWrap: true,
    );
  }
}
