import 'package:bloc/bloc.dart';
import 'package:clinico/view_model/cubuit/get_categore_from_firebase_cubuit/states.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../model/bought_devices.dart';
import '../../../model/device.dart';
import '../../../model/devices_category.dart';

class GetCategersCubit extends Cubit<GetCategourStates> {
  GetCategersCubit() : super(AuthInitializedStates());

  static GetCategersCubit get(context) =>
      BlocProvider.of<GetCategersCubit>(context);

  List<DevicesCategory> Categorus = [];
  List<Device> devices = [];

  Future<void> getCategorus() async {
    emit(GetCategourStatesLoding());
    await FirebaseFirestore.instance
        .collection("DevicesCategories")
        .get()
        .then((value) {
      value.docs.forEach((element) {
        element.reference.collection("Devices").get().then((value) {
          print(devices.length.toString());
          devices.add(Device.fromJson(element.data()));
          Categorus.add(DevicesCategory.fromJson(element.data()));
        }).catchError((error) {});
        emit(GetCategourStatesSuccess());
      });
    }).catchError((error) {
      emit(GetCategourErrorStates(error.toString()));
    });
  }
}
