import 'package:bloc/bloc.dart';
import 'package:clinico/view_model/cubuit/get_producat/states.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../model/device.dart';

class GetProductCubit extends Cubit<GetProductStates> {
  GetProductCubit() : super(AuthInitializedStates());

  static GetProductCubit get(context) =>
      BlocProvider.of<GetProductCubit>(context);
  List<Device> devices = [];
  CollectionReference? devicesCategoriesRef;

  Future<void> getDataProdact() async {
    await FirebaseFirestore.instance.collection("DevicesHistory").get().then((value) {
      print(devices.length.toString());
      value.docs.forEach((element) {
        devices.add(Device.fromJson(element.data()));
      });
      emit(GetDataProductStatesLoading());
    }).catchError((error) {
      emit(GetDataProductErrorStates(error.toString()));
    });
  }
// get data form cat
}
